# Trabajo Práctico 2

Las *computadoras* se disñan como una serie de niveles, cada uno construido sobre procesadores. Los aspectos de implementación, como el tipo de tecnología de chips empleado para implementar a memoria, no forman parte de la arquitectura. La **arquitectura de las computadoras** es el estudio del diseño de las partes de un sistema de cómputo que los programadores pueden ver.