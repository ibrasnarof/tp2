# Oranización estructurada de una computadora moderna

Casi todas las computadoras modernas constan de dos o más niveles, y pueden llegar a existir máquinas con hasta seis niveles. Los niveles de una computadora moderna son los siguientes:

**Nivel 0 / Nivel de lógica digital**: los objetos integrantes se llaman *compuertas*, y éstas pueden modelarse con exactitud como dispositivos digitales. Podemos combinar pocas compuertas para formar la memoria de un bit, capaz de almacenar un 0 o un 1.

**Nivel 1 / Nivel de microarquitectura**: se ve una colección de 8 a 32 registros que forman una memoria local y un circuito llamado *ALU*, que puede efectuar operaciones aritméticas sencillas. Los resgitros se conectan a la ALU para formar una *trayectoria de datos* por donde fluyen los datos.

**Nivel 2 / Nivel de arquitectura del conjunto de instrucciones (ISA)**: cada fabricante de computadoras realiza un manual de instrucciones del nivel ISA, describiendo las instrucciones que el microprograma o los circuitos en ejecución en hardware ejecutan de forma interpretativa.

**Nivel 3 / Nivel de máquina del sistema operativo**: es un nivel híbrido ya que todas las instrucciones de su lenguaje están también en el nivel ISA. Algunas de las instrucciones de este nivel son interpretadas por el sistema operativo y otras son interpretadas directamente por el microprograma.

**Nivel 4 / Nivel de lenguaje ensamblador**: este nivel ofrece a las personas un método de escribir programas para los niveles 1, 2 y 3 en una forma tan incomprensible como los lenguajes de máquinas virtuales. El programa que realiza la traducción se llama *ensamblador*.

**Nivel 5 / Nivel de lenguaje orientado hacia problemas**: consta de lenguajes diseñados (lenguajes de alto nivel) para ser usados por programadores de aplicaciones para resolver problemas. Los programas escritos en estos lenguajes generalmente se traducen a lenguajes de nivel 3 o 4 con traductores llamados *compiladores*.