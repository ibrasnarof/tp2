# Git

## Punto 1b

**1** Un software de control de versiones es un programa que gestiona los cambios en el código de trabajo, historial y evolución del mismo. En otras palabras, funciona como una “máquina del tiempo” que permite al usuario navegar a diferentes versiones del proyecto. Por otro lado, tiene una función de crear ramas (branches), dejando intacta la versión original y trabajar en paralelo. Cuando uno termina, lo une a la rama principal (máster). [Link](https://medium.com/@janpoloy/qu%C3%A9-es-y-para-que-sirve-git-3fd106e6e137)

**2** Los repositorios son las carpetas principales donde se encuentran almacenados los archivos que componen el proyecto. Que un repositorio sea público significa que éste es accesible para todos los usuarios que deseen acceder al mismo. Mientras que un repositorio privado está únicamente disponible para las personas autorizadas que quieran entrar. [Link](https://medium.com/@janpoloy/qu%C3%A9-es-y-para-que-sirve-git-3fd106e6e137)

**3** El árbol de Merkle es una estructura de datos estratificada para relacionar los nodos con una única raíz relacionada con éste. Para lograr esto, cada nodo debe estar identificado con un identificador único, llamado “hash”. El funcionamiento consiste es resumir estos datos en un sólo bloque de datos verificable. La ventaja que tiene es la eficiencia en la sincronización de los datos y en la verificación de éstas grandes cantidades de información.
El uso de los árboles de Merkle en el Git está relacionado con un sistema de seguimiento de cambios en un repositorio o espacio en donde se almacenan los archivos. Cada cambio realizado pasa por un proceso de “hashing”, generando un “hash” único de dicho espacio de trabajo y recibiendo el nombre de “commit”. [Link](https://agorachain.org/arboles-merkle-usos/)

**4** Las copias locales, a diferencia de las copias remotas, son versiones de los datos o proyectos que se encuentran en la PC propia de cada usuario. Mientras que las copias remotas los proyectos están hospedados en internet o en cualquier otra red.

**5** Un commit es el estado de un proyecto en un determinado momento y sirve para ver los cambios que se fueron realizando con el transcurso del tiempo. Esto debería realizarse cuando uno realiza un cambio en el proyecto, acompañando al mismo con un “hash” que lo identifica y un mensaje con los cambios que contiene. [Link](https://medium.com/@janpoloy/qu%C3%A9-es-y-para-que-sirve-git-3fd106e6e137)

**6** Para volver a una versión anterior de un trabajo, sabiendo que lo que uno avanzó debe ser modificado, se debe utilizar el comando “git checkout”, mostrando el estado en el que se encontraba el último commit.
Para que uno quiera volver aún más atrás en el tiempo, primero se debe comprobar en el registro log de ese directorio de git los commits realizados para encontrar la versión deseada. [Link](https://victorhckinthefreeworld.com/2016/07/28/git-recuperar-un-archivo-o-todo-el-repositorio-a-una-version-anterior/)

**7** Las ramas (o branches) son bifurcaciones en un instante de tiempo, generando una línea de desarrollo alterna en el transcurso de la historia del repositorio. Esto permite crear features, arreglar bugs, experimentar sin afectar la versión estable del proyecto, además de realizar nuevas funcionalidades o corregir errores. [Link 1](https://medium.com/@janpoloy/qu%C3%A9-es-y-para-que-sirve-git-3fd106e6e137) [Link 2](https://es.wikipedia.org/wiki/Control_de_versiones)


# Markdown

## Punto 2b

**1** El lenguaje Markdown es un lenguaje creado por John Gruber que facilita la aplicación de formato a un texto. El Markdown puede emplearse para cualquier tipo de texto y es una herramienta de software que convierte este lenguaje en uno de tipo HTML válido.[Link](https://www.genbeta.com/guia-de-inicio/que-es-markdown-para-que-sirve-y-como-usarlo)

**2** El lenguaje Markdown sirve para editar textos y convertirlos directamente en formato HTML. Para ello se encuentran diversos elementos de sintaxis para realizar una mejor presentación de los textos escritos.
Se encuentran muchas ventajas en la utilización de este lenguaje, como por ejemplo una escritura para rápida y cómoda para la web. Sabiendo mínimamente la sintaxis, uno puede realizar los textos de manera más rápida, resultando además más fácil de leer. Por otra parte, es ideal para utilizarla en dispositivos móviles ya que también es más rápido y ágil, especialmente si a medida que se escribe, se va aplicando el formato. A su vez, utilizando únicamente un bloc de notas es más que suficiente para emplear el Markdown. [Link](https://www.genbeta.com/guia-de-inicio/que-es-markdown-para-que-sirve-y-como-usarlo)
