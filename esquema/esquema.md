Creación de la rama "tema1"

```mermaid
sequenceDiagram
participant M as master
participant T as tema1
M->>T: Cambio de rama master a rama tema1
Note right of T: Se crea la carpeta <br/> "tema-1" y archivo <br/> "tema-1.md"
Note right of T: Se agrega un texto <br/> en el archivo <br/> "tema1.md" y se <br/> suben los cambios a <br/> la rama tema1
T->>M: Cambio de rama tema1 a master
M->>T: Cambio de rama master a tema1
Note right of T: Se agrega más texto <br/> en el archivo <br/> "tema1.md" y se <br/> suben los cambios a <br/> la rama tema1
Note right of T: Se realiza un merge <br/> de la rama tema1 a <br/> la master
T->>M: Cambio de rama tema1 a master
Note left of M: Se verifica las <br/> modificaciones <br/> realizadas