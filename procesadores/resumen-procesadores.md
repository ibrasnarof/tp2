# Organización básica de una computadora básica (Modelo de Von Neumann)

Una computadora digital consiste en un sistema de procesadores interconectados, memorias y dispositivos de entrada/salida.

La **CPU** es la unidad central de procesamiento, el cual es el “cerebro” de la computadora. La función de esta unidad es ejecutar los programas almacenados en la memoria principal, buscando y examinando instrucciones para luego ser efectuadas una tras otra.

Los componentes de la CPU están conectados por un **bus de datos**, que es una colección de alambres paralelos para transmitir direcciones, datos y señales de control. Estos buses pueden ser externos a la CPU, al conectarse con la memoria y los dispositivos de entrada/salida; sin embargo pueden ser internos moviendo los datos entre los componentes internos del microprocesador.

La CPU está compuesta por varias partes, como la **unidad de control**. Esta unidad de control es la encargada de buscar las instrucciones de la memoria principal y determinar su tipo.

Por otra parte, se encuentra la unidad de aritmética y lógica (**ALU**): Esta unidad es un circuito electrónico encargado de realizar operaciones como suma, resta y AND booleano necesarias para ejecutar instrucciones, produciendo un resultado en el registro de salida. El contenido de este registro de salida se envía a un registro y, si se desea, posteriormente se guarda en el acumulador.

Los **registros** son pequeñas áreas en la CPU, siendo éstas las áreas de memoria más rápidas en la computadora. El registro más importante es el contador de programa (PC), que apunta a la posterior instrucción debe buscarse para ejecutarse. Otro registro importante es el registro de instrucciones (CIR), conteniendo la instrucción que se está ejecutando. A su vez, se encuentran otros tipos de registros como el registro de acceso a la memoria (MAR), el registro de datos de memoria (MRD), el registro de estado (SR) y el registro de interrupción (IR).

Con respecto al registro MAR, ésta es la encargada de guardar cualquier dirección de memoria que vaya a ser utilizada en la CPU; mientras que el registro MRD guarda el dato actual que ha sido buscado en la memoria o información que está esperando ser escrita en la memoria.

El registro SR es un número de 8 bits que guarda flags sobre las operaciones que está realizando la CPU; entre tanto el registro IR está relacionado con los periféricos de entrada/salida ya que es el encargado de almacenar los detalles de las señales que se reciben en el procesador por parte de estos.

Por otra parte, los R’s son registros con propósitos generales que almacenan direcciones de memoria o dato. Un ejemplo de estos podría ser almacenar los contenidos de algún registro cuando haya alguna interrupción.

El **camino de datos** consiste en los registros, la ALU y varios buses que conectan los componentes. Los registros alimentan dos registros de entrada de la ALU y éstos además contienen las entradas de la ALU mientras esta unidad está realizando cálculos.

Casi todas las instrucciones pueden clasificarse en dos categorías. Las instrucciones  **registro-memoria** permiten buscar palabras de la memoria a los registros, utilizándose como entradas de la ALU en instrucciones posteriores. A su vez se permite almacenar el contenido de un registro en una memoria. Por otro lado, las instrucciones **registro-registro** busca dos operando de los registros, son colocados en la entrada de la ALU, y se realizan operaciones con ellos para luego el resultado ser ubicado en uno de los registros.

El **ciclo de camino de datos** es el corazón de casi todas las CPU, siendo éste el proceso de hacer pasar dos operando por la ALU y almacenar el resultado obtenido. Este ciclo define lo que la máquina puede realizar y cuánto más rápido sea, más rápidamente la máquina opera.

La **memoria principal**, también conocida como almacenamiento de acceso inmediato (IAS), es utilizada para almacenar datos e instrucciones antes de ser enviado al procesador. Como ejemplo de memoria principal se podría mencionar la memoria RAM (Random Access Memory) o la memoria ROM (Read Only Memory). Este tipo de memoria tiene como característica de ser volátil, de ser rápida y de ser un tipo de memoria flash.

Los **periféricos de entrada/salida** son aparatos o dispositivos  independientes que se conectan a la CPU de una computadora. Estos periféricos son unidades de hardware externos y muchos de ellos sirven como memoria auxiliar para almacenar información, como lo son los discos duros o memorias USB. Para conectarse con la CPU, existen controladores de entrada/salida y todo dispositivo requiere su propio controlados.


## Bibliografía

- Tanenbaum, Andrew S. *Organización de computadoras: un enfoque estructurado*. Ed. Pearson Education. México: 2000, Séptima Edición
- Material brindado por la cátedra